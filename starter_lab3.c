#include <stdio.h>
#include <stdlib.h>

// function prototypes
int get_int_value( char *prompt );
void example_basic_swap( int var_a, int var_b );

int main( )
{
	int first_number, second_number;
      
	// prompt and read the numbers in from the keyboard.
	first_number  = get_int_value( "Enter first integer value:" );
	second_number = get_int_value( "Enter second integer value:" );

	// show how the basic swap works.
	example_basic_swap( first_number, second_number );

	// show how the described swap works.
	// example_no_temp_swap( first_number, second_number );
      
	return EXIT_SUCCESS;
}

int get_int_value( char *prompt )
{
	int v1;

	printf( "%s ", prompt );
	scanf( "%d", &v1 );
    
	// Echo print our input to ensure we've read it correctly from the user
	printf( "\nEcho printing, read in the number = %d\n\n", v1 );

	return ( v1 );
}

void example_basic_swap( int var_a, int var_b )
{
	int temporary_var;

	// Display what the numbers are now.
	printf( "The numbers before swap are ( %d, %d ).\n", var_a, var_b );

	// Value of var_a is assigned to temporary_var so we can change the value of var_a.
	temporary_var = var_a;

	// Value of var_b is assigned to var_a, overwriting var_a (but we have a copy in temporary_var!)         
	var_a = var_b;

	// Value of temporary_var (which contains the initial value of var_a) is assigned to var_b;
	var_b = temporary_var;

	// Display what the numbers are after the swap.
	printf( "The numbers after swap are ( %d, %d ).\n", var_a, var_b );

	return;
}
